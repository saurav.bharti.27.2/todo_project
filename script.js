
let text_box = document.querySelector('#text_box')

let submit_btn = document.getElementById("submit_btn")

let task_container = document.getElementsByClassName('task_container')[0]

let total_tasks_count = 0;

let tasks_completed_count = 0;

let array_of_detail= []

submit_btn.onclick = function(){

    push_tasks_in_array()
    
}
document.getElementsByClassName('input_container')[0].addEventListener('keydown', function(event){

    if(event.key == 'Enter'){
        push_tasks_in_array()
    }

})



let maximum_id = 0;

function add_previous_tasks(){

    Object.entries(localStorage).map((task_already_there, index, arr) => {

        let task_id = Number(task_already_there[0])
        let [task_name, task_status] = JSON.parse(task_already_there[1]);

        array_of_detail.push([task_id, task_name, task_status])

        maximum_id = Math.max(maximum_id, task_id)
        
    })

    localStorage.clear()

    iterate_tasks('all')
}

document.addEventListener('DOMContentLoaded', function() {
    
    add_previous_tasks()
})


function push_tasks_in_array(){

    let task_information = text_box.value.trim()

    maximum_id += 1;

    if(task_information != ''){

        array_of_detail.push([maximum_id, task_information, 'pending'])
    
        iterate_tasks('all')
    }
}

function iterate_tasks( given_task_status ){

    total_tasks_count = 0
    tasks_completed_count = 0
    localStorage.clear()


    let parent = document.getElementById('task_container')
            
    while(parent.firstChild){
        parent.removeChild(parent.firstChild)
    }

    array_of_detail.map((current_task, index, array) =>{

        
        const [task_id, task_information, task_status] = current_task;
        
        total_tasks_count+= 1;

        if(task_status === 'fulfilled') 
            tasks_completed_count += 1

        if(given_task_status == 'all'){

            each_task_iterator(task_id, task_information, task_status)

        }else if(given_task_status == 'pending'){
            
            if(task_status == 'pending'){

                each_task_iterator(task_id, task_information, task_status)

            }

        }else if(given_task_status == 'none'){

            localStorage.clear()
            array_of_detail = []

            total_tasks_count = 0
            tasks_completed_count = 0
        } 
        
        
    })
    document.getElementsByClassName('total_tasks')[0].innerText = total_tasks_count
    document.getElementsByClassName('completed_tasks')[0].innerText = tasks_completed_count

}




function delete_the_task(main_task_container, task_container, checkbox, task_id){
    
    total_tasks_count-=1;
    
    if(checkbox.checked)
        tasks_completed_count -=1;

    localStorage.removeItem(task_id)
    
    // ************* Updating the array
    array_of_detail.forEach((current_task, index, array)=>{

        if(parseInt(current_task[0]) === parseInt(task_id) ){
            array.splice(index, 1);
        }

    })

    iterate_tasks('all')
    
}

function edit_the_task(task_detail, edit_emote, delete_emote, checkbox){


    task_detail.contentEditable = false;
    delete_emote.style.display = 'block'
    edit_emote.style.display = 'none'  
    
    array_of_detail.forEach((current_task, index, array) => {
        if(parseInt(current_task[0]) == parseInt(task_detail.id) ){
            array[index] = [ current_task[0], task_detail.innerText, (checkbox.checked ? 'fulfilled' : 'pending')]
        }
    })

    iterate_tasks('all')
    
}


function task_checklist(checkbox, task_detail){

    if(checkbox.checked){

        task_detail.style.textDecoration = 'line-through'
        localStorage.setItem(task_detail.id , JSON.stringify([task_detail.innerText, "fulfilled"]))

    }else{

        task_detail.style.textDecoration = 'none'
        localStorage.setItem(task_detail.id , JSON.stringify([task_detail.innerText, "pending"]))

    }

    
    array_of_detail.forEach((current_task, index, array) => {
        if(current_task[0] == task_detail.id){
            array[index] = [ current_task[0], current_task[1], (checkbox.checked ? 'fulfilled' : 'pending')]
        }
    })

    iterate_tasks('all')
}



let flag=0;

document.getElementsByClassName('main_container')[0].addEventListener('click', function(event){

    if(event.target.tagName == 'BUTTON'){


        if(event.target.id == "remove_all_btn"){
            
            iterate_tasks('none')

        }
        else if(event.target.id == 'pending_task_btn'){

            iterate_tasks('pending')

        }
        else if(event.target.id == 'all_tasks_btn' ){
            
            iterate_tasks('all')
            
        }
    }
})



function each_task_iterator( task_id, task_information, task_status){
    
    let task_detail = document.createElement('p')
        task_detail.className = 'task_detail' 
        task_detail.id = task_id
        task_detail.innerText = `${task_information}`
        

        let checkbox = document.createElement('input')
        checkbox.type = 'checkbox'
        checkbox.className = "check_box"

        if(task_status == 'fulfilled'){
            checkbox.checked = true;
            task_detail.style.textDecoration = 'line-through'
        }else{
            task_detail.style.textDecoration = 'none'
        }

        let task =document.createElement('div')
        task.className = 'task'

        task.appendChild(checkbox)
        task.appendChild(task_detail)

        let edit_emote = document.createElement('button')
        edit_emote.className = "edit_button"
        edit_emote.id = "edit_button"
        edit_emote.style.display = 'none'
        edit_emote.innerHTML = 'Update'

        let delete_emote = document.createElement('i')
        delete_emote.className = "fa-solid fa-xmark"
        delete_emote.id = "delete_button"
        delete_emote.style.display = "block"

        task.appendChild(edit_emote)
        task.appendChild(delete_emote)
        

        let main_task_container = document.createElement('div')
        main_task_container.className = 'main_task_container'

        main_task_container.appendChild(task)

        task_container.appendChild(main_task_container)

        localStorage.setItem(task_detail.id, JSON.stringify([task_information, (task_status == 'fulfilled' ? 'fulfilled' : 'pending' ) ]))

        text_box.value= ''

        checkbox.onclick = function(){

            task_checklist(checkbox, task_detail)

        }

        window.addEventListener('click', function(event) {
            if (!document.getElementById(`task_container`).contains(event.target)) {
                
                task_detail.contentEditable = false;
                delete_emote.style.display = 'block'
                edit_emote.style.display = 'none'
                task_detail.blur(); 
                
                
                if(localStorage.getItem(task_detail.id) !== null){
                    localStorage.setItem( task_detail.id, JSON.stringify([task_detail.innerText, (checkbox.checked ? 'fulfilled' : 'pending')]))
                }
            }
        })
        task_detail.addEventListener('keydown', function(event){
            
            if(event.key=== 'Enter'){
                task_detail.contentEditable = false;
                delete_emote.style.display = 'block'
                edit_emote.style.display = 'none'
                task_detail.blur(); 

                
                if(localStorage.getItem(task_detail.id) !== null){
                    localStorage.setItem( task_detail.id, JSON.stringify([task_detail.innerText, (checkbox.checked ? 'fulfilled' : 'pending')]))
                }
            }
        })

        edit_emote.onclick = function(){

            edit_the_task(task_detail, edit_emote, delete_emote, checkbox)
            
        }
        
        delete_emote.onclick = function(){

            delete_the_task(main_task_container, task_container, checkbox, task_detail.id);

        }

        main_task_container.ondblclick = function(){
            
            task_detail.contentEditable = true
            task_detail.focus();
            delete_emote.style.display = 'none'
            edit_emote.style.display = 'block'

        }


       

}

